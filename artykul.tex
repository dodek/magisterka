\documentclass{article}

\usepackage[T1]{fontenc}
%\usepackage{polski}
\usepackage[utf8]{inputenc}
\usepackage{anysize}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage[dvipsnames]{xcolor}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{hyperref}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing}
\usepackage{verbatim}
%\usepackage{unicode-math}
\usepackage{mathtools}
\author{Adam Michalik
  \footnote{Praca wykonana przy wsparciu projektu
    ,,Secant varieties, computational complexity, and toric
    degenerations'' realizowanego w ramach programu Homing Plus
    Fundacji na rzecz Nauki Polskiej, współfinansowanego z Funduszu
    Rozwoju Regionalnego Unii Europejskiej.}}

\title{Finding equations of secant varieties to Veronese embeddings
  of $\P^3$ using null-correlation bundle}

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem*{proposition*}{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{remark}[theorem]{Remark}
\newcommand\Spec{\operatorname{Spec}}
\newcommand\Proj{\operatorname{Proj}}
\newcommand\Pic{\operatorname{Pic}}
\newcommand\rad{\operatorname{rad}}
\newcommand\rank{\operatorname{rank}}
\newcommand\im{\operatorname{im}}
\newcommand\id{\operatorname{id}}
\newcommand\sheafhom{\mathcal{H}om}
\newcommand\coker{\operatorname{coker}}
\newcommand\codim{\operatorname{codim}}
\newcommand\Hom{\operatorname{Hom}}
\newcommand\Ext{\operatorname{Ext}}
\newcommand\sspan{\operatorname{span}}
\newcommand\chark{\operatorname{char}}
\renewcommand\O{\mathcal{O}}
\renewcommand\P{\mathbb{P}}
\newcommand\A{\mathbb{A}}
\newcommand\C{\mathbb{C}}
\newcommand\I{\mathcal{I}}
\newcommand\Z{\mathbb{Z}}
\newcommand\N{\mathbb{N}}
\newcommand\E{\mathcal{E}}
\newcommand\F{\mathcal{F}}
\newcommand\GG{\mathbb{G}}
\newcommand\LL{\mathcal{L}}
\newcommand\p{\mathcal{P}}

\begin{document}
\maketitle
%tu idzie streszczenie na strone poczatkowa
\begin{abstract}
  We find new classes of equations of secant varieties of Veronese
  embeddings of $\P^3$, by applying the method described by Joseph
  Landsberg and Giorgio Ottaviani in 2011 to the null-correlation
  bundle on $\P^3$. We also give a SAGE library which is useful in
  explicit calculations, and can be adapted to work with different
  bundles on projective spaces.
\end{abstract}

\section{Introduction}
Let $X \subset \P V$ be an irreducible projective variety. Any two
different points $x_1, x_2 \in X$ span a secant line $l = \langle
x_1, x_2 \rangle \subset \P V$. Consider the union of all such lines,
\begin{equation*}
  S_2(X) = \bigcup_{x_1, x_2 \in X} \langle x_1, x_2 \rangle \subset \P V.
\end{equation*}
It does not necessarily form a variety, but we can consider a smallest
variety containing it, that is, its Zariski closure
$\sigma_2(X) = \overline{S_2(X)}$. What are the equations describing
variety $\sigma_2(X)$?

More generally, we can consider Zariski closure of the union of spans
of $k$ different points in $X$:
\begin{equation*}
  \sigma_k(X) = \overline{\bigcup_{x_1, \ldots, x_k \in X} \langle
    x_1, \ldots, x_k \rangle}.
\end{equation*}
Varieties defined in this way are called \emph{secant varieties of
  X}. Studying them is a classical problem, and dates back to the
Italian school in 19th century. They have been recently enjoying a
renewed interest due to their use in solving problems in algebraic
complexity theory, algebraic statistics, and other fields. For a
survey of applications, see e.g. \cite[Chapter 1]{Landsberg2012}.

One of the most important problems in the study of secant varieties is
finding equations describing them. Not much is known about equations
of varieties of secants to arbitrary varieties, and most results give
equations for varieties of secants to some specific class of varieties
-- for instance, secants to Segre or Veronese embeddings, or some
combination of the two.

In this paper we are working on the equations cutting out secant
varieties to Veronese embeddings of $\P^3$. Our aim is to use
techniques from \cite{landott} to find equations cutting out secant
varieties. We shall briefly describe the idea.

Let $L$ be a very ample line bundle on $X$. Denote $V = H^0(L)$. We
have a natural embedding:
\begin{equation*}
  X \hookrightarrow \P V^* = \P H^0(L)^*, \quad x
  \mapsto \{ \phi \in H^0(L) : \phi(x) = 0 \}.
\end{equation*}
To find equations describing $\sigma_k(X)$, authors \cite{landott}
give the following method. Let $E$ be a vector bundle on $X$ of rank
$e$. For any $v \in H^0(L)^*$ we have a linear map
\begin{equation*}
  A^E_v: H^0(E) \to H^0(\sheafhom(E, L))^*, \quad A^E_v(s)(\phi) = v(\phi(s)).
\end{equation*}
Define:
\begin{equation*}
  Rank_k(E) = \P \{ v \in V: \rank A^E_v \leq ek \}
\end{equation*}
In \cite[Prop 5.1.1]{landott}, authors prove the following
\begin{proposition*}
  Let $X \subset \P V = \P H^0(L)^*$ be a variety, and $E$
  be a rank $e$ vector bundle on it. Then:
  \begin{equation*}
    \sigma_r(X) \subset Rank_r(E).
  \end{equation*}
  In other words, minors of size $re + 1$ of $A^E_v$ give equations
  for $\sigma_r(X)$.
\end{proposition*}

As a bundle $E$ we will use the null-correllation bundle on $\P^3$ and
its twists. This choice allows us to characterize certain secant
varieties, for example $\sigma_5(\nu_4(\P^3))$, as one of the
irreducible components of the $Rank_r(E)$ variety defined above. 

We will be working over an algebraically closed field of
characteristic 0.

\section{Null-correlation bundle and its presentation}
In this section we define the null-correlation bundle, and show its
basic properties. We will also exhibit an explicit presentation of
null-correlation bundle, which will later be useful in explicit
calculations.

Unless stated otherwise, all bundles and sheaves are on $X = \P^3 =
\Proj k[x_0, x_1, x_2, x_3]$, where $k$ is algebraically closed field
of characteristic 0.

\subsection{Construction}

Let $\omega: \O(1)^4 \to \O(2)$ be given as
\begin{equation*}
  \omega(s_0, s_1, s_2, s_3) = x_1 s_0 - x_0 s_1 + x_3 s_2 - x_2 s_3
\end{equation*}
Denote by $F$ the coherent sheaf being the kernel of $\omega$. Since
$\omega$ is surjective, $F$ is locally a free sheaf of rank $3$,
fitting into an exact sequence:
\begin{equation}\label{eq:omega-seq}
  0 \to F \to \O(1)^4 \xrightarrow{\omega} \O(2) \to 0
\end{equation}

Define $t: \O \to \O(1)^4$ by $t(1) = (x_0, x_1, x_2, x_3)$. Notice
that $\omega \circ t = 0$, thus $t$ factors through $F$. Clearly $t$
is injective. Let $E$ fit in the exact sequence:
\begin{equation}
  \label{eq:ofe-seq}
  0 \to \O \xrightarrow{t} F \to E \to 0
\end{equation}

The sheaf $E$ is also locally free: notice that cokernel of $t: \O \to
\O(1)^4$ is $T \P^3$ -- this is just the classical Euler sequence:
\begin{equation*}
  0 \to \O \xrightarrow{t} \O(1)^4 \to T \P^3 \to 0.
\end{equation*}

Since $\omega \circ t = 0$, we have an induced surjection of locally
free sheaves $\bar{\omega}: T \P^3 \to \O(2)$. Kernel of such
surjection is also locally free -- indeed, it is $E$. Since rank of $T
\P^3$ is $3$, and $\O(2)$ is line bundle, $E$ is rank $2$.

\subsection{Global sections of $E$}
We will describe global sections of $E$. Since $H^1(\O) = 0$, from the
long exact sequence corresponding to sequence (\ref{eq:ofe-seq}) we
deduce that the global sections of $E$ are the global sections of $F$
modulo sections comming from $\O$ by $t$. On the other hand, from
sequence (\ref{eq:omega-seq}) we see that we can identify global
sections of $F$ with global sections $(s_0, s_1, s_2, s_3) \in
H^0(\O(1)^4)$ satisfying
\begin{equation}
  \label{eq:omega-zero}
  x_1 s_0 - x_0 s_1 + x_3 s_2 - x_2 s_3 = 0.
\end{equation}
Since $H^0(\O(1)) = \mathrm{span} \langle x_0, x_1, x_2, x_3 \rangle$,
the global sections $H^0(\O(1)^4)$ can be identified with 4x4 matrices,
where $i$-th column contains coefficients at $x_0, x_1, x_2, x_3$ in
$i$-th direct summand of $H^0(\O(1)^4)$. Condition (\ref{eq:omega-zero})
says that $H^0(F)$ consists of the matrices of the form:
\begin{equation}
  \label{eq:h0-f}
  \begin{pmatrix}
    a & 0 & f & -e \\
    0 & a & -c & d \\
    d & e & b & 0 \\
    c & f & 0 & b
  \end{pmatrix}.
\end{equation}
The image of $t: H^0(\O) \to H^0(\O(1)^4)$ is the space of scalar matrices, so
$H_0(E) = H^0(F)/\im t$ can be identified with matrices of the form
(\ref{eq:h0-f}) and trace 0, that is, the set of matrices of the form:
\begin{equation}
  \label{eq:h0-e}
  \begin{pmatrix}
    a & 0 & f & -e \\
    0 & a & -c & d \\
    d & e & -a & 0 \\
    c & f & 0 & -a
  \end{pmatrix}.
\end{equation}

Using similar method we can obtain a description of $H^0(E(k))$ for $k \in \Z$: twist
(\ref{eq:ofe-seq}) by $k$ to obtain
\begin{equation}
  \label{eq:ofe-seq-k}
  0 \to \O(k) \xrightarrow{t} F(k) \to E(k) \to 0
\end{equation}
Again, $H^1(\O(k)) = 0$ (because we are working on $\P^3$), so it is
enough to know the description of global sections of $F(k)$. From the exact sequence
\begin{equation}
  \label{eq:f-twisted-seq}
  0 \to F(k) \to \O(k+1)^4 \xrightarrow{\omega(k)} \O(k+2) \to 0
\end{equation}
we see that global sections of $F(k)$ can be identified with sections
$(s_1, s_2, s_3, s_4) \in H^0(\O(k+1)^4)$ satisfying
(\ref{eq:omega-zero}). Applying standard methods to the long exact
sequences associated to (\ref{eq:f-twisted-seq}) and
(\ref{eq:ofe-seq}), one can obtain dimension of $H^0(E(k))$:

\begin{proposition}
  Dimension of the space of global sections of $E(k)$ for $k \in
  \mathbb{Z}$ is described by the following formula:
\begin{eqnarray*}
  h^0(E(k)) &=& 4 h^0(\O(k+1)) - h^0(\O(k+2)) - h^0(\O(k)) \\
  &=& 4{k+4 \choose 3} - {k+5 \choose 3} - {k+3 \choose 3} \\
  &=& \frac{1}{3}\left(k^3 + 9k^2 + 23k + 15\right)
\end{eqnarray*}
\end{proposition}

We can calculate the dimension easily for small $k$.
\begin{table}[h]
  \centering
\begin{tabular}[c]{c|c|c|c|c|c|c|c|c|c|c}
  k & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\ \hline
 $h^0(E(k))$ & 5 & 16 & 35 & 64 & 105 & 160 & 231 & 320 & 429 & 560
\end{tabular}
  \caption{Dimensions of space of global sections of $E(k)$ for small $k$}
  \label{fig:h0-ek-dim}
\end{table}

We will also need the following fact:
\begin{proposition}
  \label{thm:e-is-gbgs}
  $E$ is generated by global sections.
\end{proposition}
\begin{proof}
  \textcolor{red}{Brakuje.}
\end{proof}

\subsection{Presentation of null-correlation bundle}
To apply methods described by \cite{landott}, in practice it is
convienient to work with vector bundles given as presentations. 

\begin{definition}
  Presentation of a vector bundle $\E$ over a projective space $\P^n$
  is a morphism:
  \begin{equation*}
    p_{\E}: L_1 \to L_0
  \end{equation*}
  where $L_1, L_0$ are direct sums of line bundles, and
  \begin{equation*}
    \im p_E = \E.
  \end{equation*}
\end{definition}

We shall now find a nice presentation of null-correlation bundle $E$.

Since $E$ is generated by global sections, we have a surjection
\begin{equation}
  \label{eq:e-gbgs-surj}
  H^0(E) \otimes \O \simeq \O^5 \to E \to 0.
\end{equation}

On the other hand, by our construction, $E$ is subbundle of $T \P^3$. Now,
on $\P^3$ there is a Koszul complex:
\begin{equation}
  \label{eq:koszul}
  0 \to \bigoplus_{0 \leq i < j < k < l\leq 3} \O(-4) \to \bigoplus_{0
    \leq i < j < k \leq 3} \O(-3) \to \bigoplus_{0 \leq i < j \leq 3}
  \O(-2) \to \bigoplus_{0 \leq i \leq 3} \O(-1) \to \O \to 0
\end{equation}
Let $U$ be the kernel of $\bigoplus_{0 \leq i \leq 3} \O(-1) \to \O$,
so that we have an exact sequence:
\begin{equation*}
  0 \to U \to \bigoplus_{0 \leq i \leq 3} \O(-1) \to \O \to 0
\end{equation*}
Since it is exact, $U$ is locally free as a kernel of surjection of
vector bundles. Consider the dual sequence:
\begin{equation*}
  0 \to \O \to \bigoplus_{0 \leq i \leq 3} \O(1) \to U^* \to 0
\end{equation*}
The map $\O \to \bigoplus_{0 \leq i \leq 3} \O(1)$ is just the trace
map $t: \O \to \O(1)^4$, so $U^* \simeq T\P^3$. Thus $U = \Omega^1$,
the sheaf of $1$-forms on $\P^3$.

Now, by exactness of Koszul complex (\ref{eq:koszul}), we have
a surjection:
\begin{equation*}
  \bigoplus_{0 \leq i < j \leq 3} \O(-2) \to U = \Omega^1.
\end{equation*}
Dually, we have an injection:
\begin{equation}
  \label{eq:tp3-in-o2}
  0 \to T\P^3 \to \bigoplus_{0 \leq i < j \leq 3} \O(2).
\end{equation}
Putting together (\ref{eq:e-gbgs-surj}) and (\ref{eq:tp3-in-o2}), we
obtain a presentation of $E$:
\begin{equation*}
  p_E: \O^5 \twoheadrightarrow E \hookrightarrow T\P^3 \hookrightarrow
  \bigoplus_{0 \leq i < j \leq 3} \O(2).
\end{equation*}

Let us write $p_E$ explicitly.

We have a following commutative diagram:
\begin{equation}
  \label{eq:pe-comm-diag}
    \begin{tikzcd}[column sep=small]
      {}& \O^5 \arrow{ld} \arrow[two
      heads]{rd} &{}  \\
      F \arrow{rr} \arrow[hook]{d} & {} & E \arrow[hook]{d} \\
      \bigoplus_i O(1) \arrow[swap, crossing over, leftarrow]{ruu}{\phi}
      \arrow{rr} \arrow{rd}{\psi} & {} & T\P^3
      \arrow[hook]{ld} \\
      {}& \bigoplus_{i<j} O(2) \arrow[crossing over,
      leftarrow]{uuu}{p_E} &{}
  \end{tikzcd}
\end{equation}
where the horizontal maps are the cokernel maps of the trace maps $\O
\hookrightarrow F$ and $\O \hookrightarrow \bigoplus_i \O(1)$, and the
map $\O^5 \to F$ is induced by the choice of liftings of the 5 global
sections of $E$ that generate $H^0(E)$. It follows that we can
calculate $p_E$ as a composition:
\begin{equation*}
  p_E: \O^5 \xrightarrow{\phi} \bigoplus_i \O(1) \xrightarrow{\psi}
  \bigoplus_{i < j} \O(2)
\end{equation*}

The map $\phi: \O^5 \to \bigoplus_i \O(1)$ is given by $1_i \mapsto
u_i$, where $1_i$ is a unit in $i$-th summand of $H^0(\O^5)$, and
$u_i$, $i = 1, \ldots, 5$ are sections which map to a basis of
$H^0(E)$. In our case, we can assume that the sections $u_i$, $i = 1,
\ldots, 5$ map to $s_a, s_c, s_d, s_e, s_f$, where $s_a \in H^0(E)$
denote a section corresponding to $a = 1$ and $c = d = e = f = 0$, and
$s_b, s_c, s_d, s_e \in H^0(E)$ analogously, as in Proposition
\ref{thm:e-is-gbgs}. Thus, $\phi: \O^5 \to \bigoplus_i \O(1)$ is given
by a matrix:
\begin{equation}
  \label{eq:e-o1-glob-sec-map}
  \begin{pmatrix}
    x_0 & x_3 & x_2 & 0 & 0 \\
    x_1 & 0 & 0 & x_2 & x_3 \\
    -x_2 & -x_1 & 0 & 0 & x_0 \\
    -x_3 & 0 & x_1 & -x_0 & 0 \\
  \end{pmatrix}
\end{equation}

As discussed earlier, the map $\psi: \bigoplus_i \O(1) \to \bigoplus_{i <
  j} \O(2)$ is dual to a Koszul complex map $\bigoplus_{i < j} \O(-2)
\twoheadrightarrow \Omega^1 \hookrightarrow \bigoplus_i \O(-1)$, which
corresponds to the following map of graded $S = k[x_0, x_1, x_2,
x_3]$-modules:
\begin{equation}
  \label{eq:diff-kosz}
  \bigoplus_{i < j} S(-2) \to \bigoplus_i S(-1), \quad e_{ij} \mapsto
  x_j e_i - x_i e_j
\end{equation}
which, as a matrix, is given by:
\begin{equation}
  \label{eq:koszul-map}
  \begin{pmatrix}
    x_1 & x_2 & x_3 & 0 & 0 & 0 \\
    -x_0 & 0 & 0 & x_2 & x_3 & 0 \\
    0 & -x_0 & 0 & -x_1 & 0 & x_3 \\
    0 & 0 & -x_0 & 0 & -x_1 & -x_2
  \end{pmatrix}.
\end{equation}
Dualizing, the map $\psi: \bigoplus_i \O(1) \hookrightarrow \bigoplus_{i <
  j} \O(2)$ is given by:
\begin{equation}
  \label{eq:koszul-map-dual}
  \begin{pmatrix}
    x_1 & -x_0 & 0 & 0\\
    x_2 & 0 & -x_0 & 0 \\
    x_3 & 0 & 0 & -x_0 \\
    0 & x_2 & -x_1 & 0 \\
    0 & x_3 & 0 & -x_1 \\
    0 & 0 & x_3 & -x_2
  \end{pmatrix}.
\end{equation}


We can now calculate the matrix of $p_E: \O^5 \to \bigoplus_{i < j}
\O(2)$ -- it is the product of (\ref{eq:koszul-map-dual}) and (\ref{eq:e-o1-glob-sec-map}):

\begin{equation}
  \label{eq:pe-as-matrix}
  \begin{pmatrix}
0 & x_1 x_3 & x_1 x_2 &  x_0 x_2 & -x_0 x_3\\
 2 x_0 x_2 & x_2 x_3+x_0 x_1 &  x_2^2 & 0 & -x_0^2\\
 2 x_0 x_3 &  x_3^2 & x_2 x_3-x_0 x_1 &  x_0^2 & 0\\
 2 x_1 x_2 & x_1^2 & 0 &  x_2^2 & x_2 x_3-x_0 x_1\\
 2 x_1 x_3 & 0 & -x_1^2 & x_2 x_3 +x_0 x_1 & x_3^2\\
 0 & -x_1 x_3 & -x_1 x_2 & x_0 x_2 & x_0 x_3
\end{pmatrix}
\end{equation}

We shall now obtain some more information about the presentation
$p_E$, and use it to obtain a better presentation.

Consider an inclusion $T\P^3 \hookrightarrow \bigoplus_{i < j}
\O(2)$. To find the cokernel, we dualize and take a look at the
kernel: the dual map $\bigoplus_{i < j} \O(-2) \to \Omega^1$ comes
from the Koszul complex map $\bigoplus_{i < j} S(-2) \to \bigoplus_i
S(-1)$, and its kernel is equal to the image of $\bigoplus_{i < j < k}
S(-3) \to \bigoplus_{i < j} S(-2)$, which is the same as cokernel of
$\bigoplus_{i < j < k < l} S(-4) \to \bigoplus_{i < j < k} S(-3)$. But
this map is the dual of the map $\bigoplus_{i} S(-1) \to S$ shifted by
$4$. Since we know that its kernel is $\Omega^1$, the cokernel of
$\bigoplus_{i < j < k < l} S(-4) \to \bigoplus_{i < j < k} S(-3)$ is
$T\P^3(-4)$. It can be illustrated by the following diagram, in which
the horizontal sequence is the Koszul complex:
\begin{equation}
  \label{eq:kosz-comp-add}
    \begin{tikzcd}[column sep=small, row sep=small]
      {} & {} & 0 \arrow{rd} & {} & 0 & {} & {} & {} & {} \\
      {} & {} & {} & T\P^3(-4) \arrow{rd}  \arrow{ru} & {} & {} & {} &
      {} & {} \\
      0 \arrow{r} & \O(-4) \arrow{r} & \displaystyle\bigoplus\limits_{i < j < k}
      \O(-3) \arrow{rr} \arrow{ru} & {} & \displaystyle\bigoplus\limits_{i < j} \O(-2)
      \arrow{rr}  \arrow{rd} & {} & \displaystyle\bigoplus\limits_i
      \O(-1) \arrow{r} & \O \arrow{r} & 0\\
      {} & {} & {} & {} & {} & \Omega^1 \arrow{rd} \arrow{ru} & {} & {} & {} &
      {} & {} \\
      {} & {} & {} & {} & 0 \arrow{ru} & {} & 0 & {} & {} & {} & {} \\
  \end{tikzcd}
\end{equation}

It follows that we have a following exact sequence of locally free sheaves:
\begin{equation*}
  0 \to T\P^3(-4) \to \bigoplus_{i<j} \O(-2) \to \Omega^1 \to 0
\end{equation*}
Dualizing, we obtain:
\begin{equation*}
  0 \to T\P^3 \to \bigoplus_{i<j} \O(2) \to \Omega^1(4) \to 0
\end{equation*}

Let $C$ be the cokernel of inclusion $E \to \bigoplus_{i < j}
\O(2)$. By previous considerations, we have the following commutative
diagram, in which all rows and columns are exact:
\begin{equation}
  \label{eq:pe-nine-diag}
    \begin{tikzcd}[row sep=small]
      {} & 0 \arrow{d} & 0 \arrow{d} & 0 \arrow{d} & {} \\
      0 \arrow{r} & E \arrow[equal]{d} \arrow{r} & T\P^3
      \arrow{r}{\omega} \arrow{d} & \O(2) \arrow{d} \arrow{r} & 0 \\
      0 \arrow{r} & E \arrow{d}\arrow{r} & \bigoplus_{i<j} \O(2)
      \arrow{d}\arrow{r} & C \arrow{d} \arrow{r} & 0 \\
      0 \arrow{r} & 0 \arrow{d}\arrow{r} & \Omega^1(4) \arrow[equal]{r}
      \arrow{d} & \Omega^1(4) \arrow{d} \arrow{r} & 0\\
      {} & 0 & 0 & 0 & {}
  \end{tikzcd}
\end{equation}
We see that $C$ is an extension of $\Omega^1(4)$ by $\O(2)$. To
understand $C$, we will calculate $\Ext^1(\Omega^1(4), \O(2)) =
\Ext^1(\Omega^1, \O(-2))$. From Euler sequence:
\begin{equation*}
  0 \to \Omega^1 \to \O(-1)^4 \to \O \to 0
\end{equation*}
we have a long exact Ext sequence:
\begin{equation*}
  \ldots \to \Ext^1(\O(-1)^4, \O(-2)) \to \Ext^1(\Omega^1, \O(-2)) \to
  \Ext^2(\O, \O(-2)) \to \ldots
\end{equation*}
We have:
\begin{equation*}
  \Ext^1(\O(-1)^4, \O(-2)) = \Ext^1(\O(-1), \O(-2))^4 = \Ext^1(\O,
  \O(-1))^4 = H^1(\O(-1))^4 = 0
\end{equation*}
\begin{equation*}
  \Ext^2(\O, \O(-2)) = H^2(\O(-2)) = 0
\end{equation*}
Thus $\Ext^1(\Omega^1(4), \O(2)) = \Ext^1(\Omega^1, \O(-2)) = 0$, and
so $C \simeq \Omega^1(4) \oplus \O(2)$.

This additional $\O(2)$ summand suggests that there is some redundancy
in $p_E$. Indeed, if we look back at the matrix
(\ref{eq:pe-as-matrix}), we see that row 1 (that is, the first summand
in $\bigoplus_{i<j} \O(2)$) is redundant. More precisely, if we
change the order of the summands in $\O^5$, so that $p_E$ is
represented by the matrix:
\begin{equation}
  \label{eq:pe-as-matrix2}
  \begin{pmatrix}
    -x_0 x_2 & -x_0 x_3 & x_1 x_2 & x_1 x_3 & 0 \\
    0 & -x_0^2 & x_2^2 & x_0 x_1 + x_2 x_3 & 2x_0 x_2 \\
    x_0^2 & 0 & x_2 x_3 - x_0 x_1 & x_3^2 & 2 x_0 x_3 \\
    x_2^2 & x_2 x_3 - x_0 x_1 & 0 & x_1^2 & 2 x_1 x_2 \\
    x_0 x_1 + x_2 x_3 & x_3^2 & -x_1^2 & 0 & 2 x_1 x_3 \\
    x_0 x_2 & x_0 x_3 & -x_1 x_2 & -x_1 x_3 & 0 \\
  \end{pmatrix}
\end{equation}
and if we compose it with an automorphism of $\O^5$ that multiplies
5th summand by $-\frac{1}{2}$, and also with an automorphism of
$\O(2)^6$ that multiplies 4th and 5th summand by $-1$, we obtain a map
$q: \O^5 \to \bigoplus_{i<j} \O(2)$ represented by a matrix:
\begin{equation}
  \label{eq:pe-as-matrix3}
  \begin{pmatrix}
    -x_0 x_2 & -x_0 x_3 & x_1 x_2 & x_1 x_3 & 0 \\
    0 & -x_0^2 & x_2^2 & x_0 x_1 + x_2 x_3 & -x_0 x_2 \\
    x_0^2 & 0 & x_2 x_3 - x_0 x_1 & x_3^2 & -x_0 x_3 \\
    -x_2^2 & x_0 x_1-x_2 x_3 & 0 & -x_1^2 & x_1 x_2 \\
    -x_0 x_1 - x_2 x_3 & -x_3^2 & x_1^2 & 0 & x_1 x_3 \\
    x_0 x_2 & x_0 x_3 & -x_1 x_2 & -x_1 x_3 & 0 \\
  \end{pmatrix}
\end{equation}
which still has the property that $\im q \simeq E$. Its first row,
however, is the transposition of the last column (up to scalar). Thus,
the projection map $\bigoplus_{i<j} \O(2) \to \O(2)^5$ that drops the
first coordinate (corresponding to $i=0, j=1$) is injective on $E$,
and so composition of this projection with $q$ has its image still
isomorphic to $E$. \textbf{From now on, we shall call this map $p_E:
  \O^5 \to \O(2)^5$}. Its matrix is:
\begin{equation}
  \label{eq:pe-as-matrix-antisym}
  \begin{pmatrix}
    0 & -x_0^2 & x_2^2 & x_0 x_1 + x_2 x_3 & -x_0 x_2 \\
    x_0^2 & 0 & x_2 x_3 - x_0 x_1 & x_3^2 & -x_0 x_3 \\
    -x_2^2 & x_0 x_1-x_2 x_3 & 0 & -x_1^2 & x_1 x_2 \\
    -x_0 x_1 - x_2 x_3 & -x_3^2 & x_1^2 & 0 & x_1 x_3 \\
    x_0 x_2 & x_0 x_3 & -x_1 x_2 & -x_1 x_3 & 0 \\
  \end{pmatrix}
\end{equation}
Note that the matrix is antisymmetric. Twisting it by $-1$ we can
obtain a self-dual presentation of $E(-1) \simeq E^*(1)$:
\begin{equation*}
  p_E(-1): \O(-1)^5 \to \O(1)^5, \quad (p_E(-1))^* = -p_E(-1).
\end{equation*}
By previous calculations, $p_E$ fits in an exact sequence:
\begin{equation}
  \label{eq:pe-loc-free-part}
  \O^5 \xrightarrow{p_E} \O(2)^5 \to \Omega^1(4) \to 0
\end{equation}
To find the kernel of $p_E$, notice that the dual map $p_E^*$ is just
$-p_E(-2)$, so if we dualize (\ref{eq:pe-loc-free-part}), we obtain:
\begin{equation}
  \label{eq:pe-loc-free-part-dual-1}
  0 \to T\P^3(-4) \to \O(-2)^5 \xrightarrow{-p_E(-2)} \O^5
\end{equation}
Now twist it by $2$ to obtain:
\begin{equation}
  \label{eq:pe-loc-free-part-dual-2}
  0 \to T\P^3(-2) \to \O^5 \xrightarrow{-p_E} \O(2)^5
\end{equation}
Since the kernel of $-p_E$ is the same as the kernel of $p_E$, we have an exact sequence:
\begin{equation}
  \label{eq:pe-ex-seq-long}
  0 \to T\P^3(-2) \to \O^5 \xrightarrow{p_E} \O(2)^5 \to \Omega^1(4) \to 0
\end{equation}

\section{Finding equations of secant
  varieties}\label{chap:finding-equations}

In this section we describe how one can use vector bundles to find
equations of secant varieties. We apply the method to the
null-correlation bundle, and give some examples where resulting rank
variety has secant variety as an irreducible component. We also
intersect the rank variety of null-correlation bundle with the
catalecticant variety for symmetric catalecticant.

\subsection{Obtaining equations from vector bundles}
Let $E$ be a vector bundle on~$X$~of rank $e$. For any $v \in
H^0(L)^*$ we have a linear map
\begin{equation*}
  A^E_v: H^0(E) \to H^0(\sheafhom(E, L))^*
\end{equation*}
defined by
\begin{equation*}
  A^E_v(s)(\phi) = v(\phi(s))
\end{equation*}
where $s \in H^0(E), \phi \in H^0(\sheafhom(E, L))$.

Define:
\begin{equation*}
  Rank_k(E) = \P \{ v \in V: \rank A^E_v \leq ek \}
\end{equation*}

The following theorem \cite[Prop 5.1.1]{landott} is the essence:
\begin{theorem}
  \label{thm:vec-minors-sec-rank}
  Let $X \subset \P V = \P H^0(L)^*$ be a variety, and $E$
  be a rank $e$ vector bundle on it. Then:
  \begin{equation*}
    \sigma_r(X) \subset Rank_r(E).
  \end{equation*}
  In other words, minors of size $re + 1$ of $A^E_v$ give equations
  for $\sigma_r(X)$.
\end{theorem}

As mentioned before, in practice, the description of $H^0(E)$ and
$H^0(\sheafhom(E, L))$ may not be easy to find and work with, and
frequently it is easier to work with presentations.

The Theorem \ref{thm:vec-minors-sec-rank} has its analogue in the
presentation setting. Take any $v \in H^0(L)^*$. We have a natural
map
\begin{equation*}
  P^E_v: H^0(L_1) \to H^0(\sheafhom(L_0, L))^*, \quad
  P^E_v(s)(\phi) = v(\phi(p_E(s))).
\end{equation*}

Theroem \ref{thm:vec-minors-sec-rank} now takes form
\cite[Prop. 8.4.1]{landott}
\begin{theorem}
  \label{thm:vec-minors-sec-rank-pres}
  Assume that maps $H^0(L_1) \xrightarrow{i} H^0(E)$ and
  $H^0(\sheafhom(L_0, L)) \xrightarrow{j} H^0(\sheafhom(E, L))$
  are surjective. Then the rank of $A^E_v$ equals the rank of $P^E_v$,
  so that the size $ke+1$ minors give equations for $\sigma_k(X)$.
\end{theorem}
\begin{proof}
  We have the following commutative diagram:
  \begin{equation*}
  \begin{tikzcd}
    H^0(L_1) \arrow{r}{P^E_v} \arrow[two heads]{d}{i} & H^0(\sheafhom(L_0, L))^* \\
    H^0(E) \arrow{r}{P^E_v} &  H^0(\sheafhom(E, L))^* \arrow[hook]{u}{j^*}
  \end{tikzcd}
  \end{equation*}
  Since by assumption $i$ and $j$ are surjective, $j^*$ is injective,
  so it follows that $\rank A^E_v = \rank P^E_v$.
\end{proof}

It is also useful to know when the rank conditions give enough
equations to cut out the secant variety. For this, there is a
following theorem:
\begin{theorem} \cite[8.4.2]{landott} \label{thm:rank-pairing-irr}
  Notations as above. Let $v \in \sigma_k(X)$.  Assume that maps
  $H^0(L_1)~ \xrightarrow{i}~ H^0(E)$ and $H^0(\sheafhom(L_0, L))~
  \xrightarrow{j}~ H^0(\sheafhom(E, L))$ are surjective. Define
  pairing:
  \begin{equation*}
    g_v: \ker P_v \otimes (\im P_v)^\perp \to H^0(L)^{**}
  \end{equation*}
  by
  \begin{equation*}
    g_v(f, h)(\phi) = P_\phi(f)(h)
  \end{equation*}
  If the rank of $g_v$ equals the codimension of $\sigma_k(X)$, then
  $\sigma_k(X)$ is an irreducible component of $Rank_k(E)$ passing
  through $[v] \in \P H^0(L)^*$.
\end{theorem}

The reason why such pairing $g_v$ appears here is the following. We
can naturally identify $H^0(L)^{**}$ with $H^0(L)$. Then for $v$ such
that $[v]$ is a smooth point of $Rank_k(E)$, the image of $g_v$ in
$H^0(L)$ is precisely the affine conormal space of $Rank_k(E)$ at
$[v]$.

\subsection{Using presentation of $E$ to find equations}
If we want to use our presentation $p_E: L_1 \to L_0$, where $L_1 =
\O^5$ and $L_0 = \O(2)^5$, we need to check that $H^0(L_1)
\xrightarrow{i} H^0(E)$ and $H^0(\sheafhom(L_0, L)) \xrightarrow{j}
H^0(\sheafhom(E, L))$ are surjective. But from
(\ref{eq:pe-ex-seq-long}) we have exact sequences:
\begin{equation}
  \label{eq:pe-part-0}
  0 \to T\P^3(-2) \to \O^5 \to E \to 0
\end{equation}
\begin{equation}
  \label{eq:pe-part-1}
  0 \to E \to \O(2)^5 \to \Omega^1(4) \to 0
\end{equation}
Applying $\sheafhom(-, \O(d))$ to (\ref{eq:pe-part-1}), which is the same
as dualizing and twisting by $d$, we get:
\begin{equation}
  \label{eq:pe-part-2}
  0 \to T\P^3(-4+d) \to \sheafhom(\O(2)^5, \O(d)) \to \sheafhom(E,
  \O(d)) \to 0
\end{equation}

Since $H^1(T\P^3(k)) = 0$ for any $k \in \Z$ (as one can easily deduce
from Euler sequence), considering long exact cohomology sequences of
(\ref{eq:pe-part-0}) and (\ref{eq:pe-part-2}), we see that assumptions
of the Theorem \ref{thm:vec-minors-sec-rank-pres} are satisfied for $p_E$ and
all of its twists. Thus, we have a following theorem:
\begin{theorem}
  \label{thm:eq-for-sec-using-ek}
  Let $p_E: \O^5 \to \O(2)^5$ be the presentation of null-correlation
  bundle, as above. Let $k \in \Z$, $d, r \in \N$ be arbitrary. Then the
  size $2r +1$ minors of the evaluation map:
  \begin{equation*}
    P^{E(k)}_v: H^0(\O(k)^5) \to H^0(\sheafhom(\O(k+2)^5, \O(d)))
  \end{equation*}
  give equations for $\sigma_r(\nu_d(\P^3))$.
\end{theorem}

\subsection{Explicit calculations for $\sigma_5(\nu_4(\P^3))$}
We shall now use Theorem \ref{thm:eq-for-sec-using-ek} to find
explicit equations for $\sigma_5(\nu_4(\P^3))$. Theorem says that we
need to use minors of size $2\cdot5 + 1 = 11$. From table
(\ref{fig:h0-ek-dim}) we see that since $h^0(E) = 5$, the map $P^E_v$
has rank $\leq 5$ for any $v \in H^0(\O(4))^*$, so we cannot possibly
obtain any new information. Because of that, instead of $E$, we shall
use $E(1)$ with presentation $p_{E(1)}: \O(1)^5 \to \O(3)^5$ -- the
matrix representing the presentation $p_{E(1)}$ of $E(1)$ is the same
as matrix of $p_E$, but we now interpret it as a map $\O(1)^5 \to
\O(3)^5$.

The corresponding $P^{E(1)}_v$ maps now is:
\begin{equation*}
  P^{E(1)}_v: H^0(\O(1)^5) \to H^0(\sheafhom(\O(3)^5, \O(4)))^*, \quad
  P^{E(1)}_v(s)(\phi) = v(\phi(p_{E(1)}(s)))
\end{equation*}
and $h^0(\O(1)^5) = h^0(\sheafhom(\O(3)^5, \O(4))) = 20$.

As a basis of $H^0(\O(k))$ we choose homogenous degree $k$ polynomials
in variables $x_0, x_1, x_2, x_3$. The basis of the dual
$H^0(\O(k))^*$ will be the dual basis -- the dual to $x_0^{a_0}
x_1^{a_1} x_2^{a_2} x_3^{a_3}$ will be denoted by $\phi_{a_0,a_1,a_2,a_3}$.

In these bases, for a given presentation $p_F: L_1 \to L_0$, we can
obtain explicit matrix of $P^{F}_\phi$ as follows \cite[8.3]{landott}:
let $L_1 = \bigoplus_{j = 1}^{m_1} \O(b_j)$, $L_0 = \bigoplus_{i =
  1}^{m_0} \O(b_i)$, and let $p_{ij} \in H^0(\O(a_i - b_j))$ denote
the map $\O(b_j) \to \O(a_i)$ given by composition:
\begin{equation*}
  \O(b_j) \to L_1 \to L_0 \to \O(a_i)
\end{equation*}
Then for any $\phi \in H^0(L)^*$, the matrix of $P^F_\phi$ is obtained
by taking a matrix of $m_1 \times m_0$ blocks, the $(i, j)$-th block
representing the catalecticant contracted by $p_{ij}$.

In Appendix \ref{chap:exp-calc} we describe, how we used the program
described in Appendix \ref{chap:sage-lib} to calculate the matrix of
$P^{E(1)}_v$ the minors of which give equations for
$\sigma_5(\nu_4(\P^3))$, as in Theorem
\ref{thm:vec-minors-sec-rank-pres}. We also calculated the rank of
the conormal pairing, as in Theorem \ref{thm:rank-pairing-irr}.

For randomly chosen general point $[v] \in \sigma_5(\nu_4(\P^3))$, the
rank of the conormal pairing is 15. Since $\sigma_5(\nu_4(\P^3))$ is
not on the exceptions list from Theorem \ref{thm:alex-hirsh}, it has
the expected dimension, which is $5\cdot3 + 5 - 1 = 19$. On the other
hand, the projective space that $\sigma_5(\nu_4(\P^3))$ is embedded in
has dimension ${{4+3} \choose 4} - 1 = 34$. The codimension of
$\sigma_5(\nu_4(\P^3))$ is thus 15, and since it is equal to the rank
of conormal pairing, by Theorem \ref{thm:rank-pairing-irr} we obtain
the following:
\begin{theorem}
  \label{thm:eqs-for-s5v4p3}
  Let $p_{E(1)}: \O(1)^5 \to \O(3)^5$ be the presentation of the
  null-correlation bundle twisted by 1, $E(1)$, given by the matrix
  (\ref{eq:pe-as-matrix-antisym}). Then $\sigma_5(\nu_4(\P^3))$ is an
  irreducible component of the variety cut out by $11 \times 11$ size
  minors of $P^{E(1)}_v: H^0(\O(1)^5) \to H^0(\O(1)^5)$
\end{theorem}

It is also easy to see that the matrix of $P^{E(1)}_v$ occuring in the
Theorem \ref{thm:eqs-for-s5v4p3} is actually skew symmetric, so
instead of $11 \times 11$ size minors, we can take $12 \times 12$
Pfaffians.

\subsection{Calculations for other secant varieties}
Similarly to $\sigma_5(\nu_4(\P^3))$, we also calculated, when
$\sigma_k(\nu_d(\P^3))$ is an irreducible component of $Rank_k(E(l))$,
where $E(l)$ is the null-correlation bundle twisted by $l$. The
calculations where performed for $3 \leq d \leq 8$ and $1 \leq l \leq
4$. The results can be found in Appendix \ref{chap:results-of-calcs}.

\subsection{Intersection with rank variety of catalecticant maps}
Classical way of finding equations of secant varieties to Veronese
embedding was by studying the catalecticant maps. These were obtained
via natural embedding $S^d V \hookrightarrow S^a V \otimes S^{d-a} V$
--~for $\phi \in S^d V$, one considers the corresponding map $\phi_{a,
  d-a}: S^a V^* \to S^{d-a} V$. If $\phi \in \sigma_r(\nu_d(\P V))$,
then $\rank \phi_{a, d-a} \leq r$. Thus, if one chooses basis of $V$
and writes down the corresponding matrix of $\phi_{a, d-a}$, its
$(r+1) \times (r+1)$ size minors give equations of $\sigma_r(\nu_d(\P
V))$. For explicit examples, and history, see \cite{geramita}.

This approach can be viewed as using the vector bundle method above --
indeed, if one chooses $X = \P V$, $L = \O(d)$, and $E = \O(a)$, the
corresponding map $A^E_v: H^0(E) \to H^0(\sheafhom(E, L)^*$ is just a
map $H^0(\O(a)) \to H^0(\O(a)^* \otimes \O(d))^*$, and $H^0(\O(a)) =
S^a V^*$, while $H^0(\O(a)^* \otimes \O(d))^* = H^0(\O(d-a))^* =
S^{d-a} V^{**} = S^{d-a} V$, and careful consideration of the maps
involved makes it clear that these are just the catalecticants
described above.

In some cases it is known that catalecticant minors cut out secant
variety as a scheme, or at least give equations for variety such that
secant variety is its irreducible component. For instance, size $5
\times 5$ minors of $\phi_{a, d-a}$ for $a = \lfloor \frac{d}{2}
\rfloor$ cut out $\sigma_4(\nu_d(\P^2))$ as a scheme. For more
examples, see table in \cite[p. 2]{landott}

However, just like the equations stemming from the null-correlation
bundle above, the catalecticant equations are not always enough --
calculating the conormal space shows that the corresponding rank
variety is sometimes too large. However, we can intersect the rank
variety of null-correlation bundle with the rank variety of
catalecticant. The conormal space to the intersection is a sum of
conormal spaces to the factors. We can therefore calculate the
conormal space to the intersection at some element of the secant
variety, and check whether its dimension is equal to the codimension
of the secant variety. When this is the case, the secant variety is an
irreducible component of the intersection.

We performed the calculation in some cases when neither
null-correlation bundle, nor symmetric catalecticants (that is, for $a
= \lfloor \frac{d}{2} \rfloor$) are by themselves enough to cut out
the secant variety. In many cases, the sum of conormal spaces had
dimension equal to the codimension of the secant variety. When this is the
case, the secant variety is an irreducible component of the
intersection.

For example:
\begin{itemize}
\item $\sigma_6(\nu_4(\P^3))$: its dimension is 23, codimension 11. We
  randomly chose some generic element $x$ of the secant variety. The
  conormal space of rank variety of null-correlation bundle twisted by
  1 at $x$ has dimension 6. The corresponding conormal space for
  catalecticant minors has dimension 10, and their sum has dimension
  11, which is equal to the codimension.
\item $\sigma_{14}(\nu_6(\P^3))$: dimension is 55, codimension is
  28. The rank variety of $E(2)$, the null-correlation bundle twisted
  by 2, has 21-dimensional conormal space at a randomly chosen element
  of the secant variety. The conormal space of catalecticant variety
  at the same point is also 21, and their sum has dimension 28, which
  is equal to the codimension.
\item $\sigma_{15}(\nu_6(\P^3))$: dimension is 59, codimension is 24.
  The conormal of null-correlation has dimension 10, the conormal of
  catalecticant has dimension 15, their sum has dimension 24, which is
  equal to the codimension.
\end{itemize}

On the other hand, there are also cases when even the two classes of
equations aren't enough:
\begin{itemize}
\item $\sigma_{16}(\nu_6(\P^3))$: dimension is 63, codimension is 20.
  At a randomly chosen element, the conormal space of the rank variety
  of null-correlation bundle twisted by 2 has dimension 3, while the
  conormal space to the catalecticant variety has dimension 10. Their
  sum has dimension 13, which is smaller than the codimension.
\end{itemize}

\begin{thebibliography}{99}
\bibitem[LandOtt11]{landott} J.M. Landsberg, Giorgio Ottaviani,
  \textit{Equations for secant varieties of Veronese and other
    varieties}, arXiv:1111.4567 [math.AG]

\bibitem[Landsberg2012]{Landsberg2012} J.M. Landsberg \textit{Tensors:
    Geometry and Applications}, American Mathematical Society, 2012
\end{thebibliography}


\end{document}