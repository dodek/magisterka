load("secant_equations.sage")

R.<x0, x1, x2, x3> = QQ[]

proj_coords = [x0, x1, x2, x3]
proj_dim = len(proj_coords) - 1
print "Working on P^%d " % proj_dim


def catalecticant_conormal(a, d_a, veronese_deg, kth_secant, v = None):
    pe = matrix([[1]])
    l1 = [a]
    l0 = [d_a]
    expected_dim = kth_secant*(proj_dim+1) - 1
    veronese_dim = binomial(proj_dim+veronese_deg, veronese_deg) - 1
    codim = veronese_dim - expected_dim

    print "---"
    print "Using phi_%d,%d" % (a, d_a)
    print "Working with s_%d(v_%d(P^%d))" % (kth_secant, veronese_deg, proj_dim)
    print "Expected dimension is %d, expected codimension %d" % (expected_dim, codim)
    pev = calculate_pev(pe, proj_coords, l1, l0, veronese_deg)

    if v == None:
        v = generic_element_of_veronese_secant(proj_coords,
                d=veronese_deg, k=kth_secant)

    print "pev rank: ", pev(**v).rank()

    return  find_rank_conormal_pairing(
        pev, v, [x0,x1,x2,x3], veronese_deg)

def null_correlation_conormal(e_twist, veronese_deg, kth_secant, v = None):
    pe = matrix([[0 , -x0^2 , x2^2 , x0*x1 + x2*x3 , -x0*x2],
        [x0^2 , 0 , x2*x3 - x0*x1 , x3^2 , -x0*x3],
        [-x2^2 , x0*x1 - x2*x3, 0 , -x1^2 , x1*x2],
        [-x0*x1 -x2*x3 , -x3^2 , x1^2 , 0 , x1*x3],
        [x0*x2 , x0*x3 , -x1*x2 , -x1*x3 , 0]])
    l1 = [e_twist]*5
    l0 = [e_twist+2]*5
    expected_dim = kth_secant*(proj_dim+1) - 1
    veronese_dim = binomial(proj_dim+veronese_deg, veronese_deg) - 1
    codim = veronese_dim - expected_dim

    print "---"
    print "Using E(%d)" % e_twist
    print "Working with s_%d(v_%d(P^%d))" % (kth_secant, veronese_deg, proj_dim)
    print "Expected dimension is %d, expected codimension %d" % (expected_dim, codim)
    pev = calculate_pev(pe, proj_coords, l1, l0, veronese_deg)

    if v == None:
        v = generic_element_of_veronese_secant(proj_coords,
                d=veronese_deg, k=kth_secant)

    return find_rank_conormal_pairing(pev, v, [x0,x1,x2,x3], veronese_deg)


import time

d = 4
k = 6
v = generic_element_of_veronese_secant(proj_coords, d, k)
cat_cnrml = catalecticant_conormal(d/2, d/2, d, k, v)
print cat_cnrml.dimension()
null_cnrml = null_correlation_conormal(1, d, k, v)
print null_cnrml.dimension()
print span(cat_cnrml.basis() + null_cnrml.basis()).dimension()
