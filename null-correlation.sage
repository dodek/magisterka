load("secant_equations.sage")

R.<x0, x1, x2, x3> = QQ[]

proj_coords = [x0, x1, x2, x3]
proj_dim = len(proj_coords) - 1
print "Working on P^%d " % proj_dim

pe = matrix([[0 , -x0^2 , x2^2 , x0*x1 + x2*x3 , -x0*x2],
    [x0^2 , 0 , x2*x3 - x0*x1 , x3^2 , -x0*x3],
    [-x2^2 , x0*x1 - x2*x3, 0 , -x1^2 , x1*x2],
    [-x0*x1 -x2*x3 , -x3^2 , x1^2 , 0 , x1*x3],
    [x0*x2 , x0*x3 , -x1*x2 , -x1*x3 , 0]])
l1 = [1,1,1,1,1]
l0 = [3,3,3,3,3]
print "Presentation is %s -> %s" % (l1, l0)
print "Presentation matrix: "
print pe

veronese_deg = 4
print "Degree of Veronese embedding: %d" % veronese_deg
pev = calculate_pev(pe, proj_coords, l1, l0, veronese_deg)
print "P^E_v:"
print pev

kth_secant = 5
expected_dim = kth_secant*(proj_dim+1) - 1
print "Working with s_%d(v_%d(P^%d)), its expected dimension is %d" \
  % (kth_secant, veronese_deg, proj_dim, expected_dim)

v = generic_element_of_veronese_secant(proj_coords,
        d=veronese_deg, k=kth_secant)
print "Calculating rank of conormal space at: "
print v

print "Conormal space has rank %d" % find_rank_conormal_pairing(
    pev, v, [x0,x1,x2,x3], veronese_deg).rank()
veronese_dim = binomial(proj_dim+veronese_deg, veronese_deg) - 1
print "Codimension of secant variety is %d - %d = %d" \
  % (veronese_dim, expected_dim, veronese_dim - expected_dim)
