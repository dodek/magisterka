def contract(m, d):
    '''
    contract(polynomial, diff) -- contract polynomial by polynomial

    Let S = k[x_0, ..., x_n] be a polynomial ring. S is naturally
    a graded S-module, but we can also introduce a nonstandard
    module structure like this -- let S' = k[y_0, ..., y_n] be a
    polynomial ring with renamed variables. Let S act on S' as if x_i
    was d/dx_i, but without differentiation constant. In other words,
    x_i acts on monomial f(y_0, ..., y_n) by reducing the degree in
    which the variable y_i occurs in f, or giving 0 if y_i doesn't
    occur in f. For example:
    x_0 * y_0^n = y_0^{n-1}
    or
    x_0 x_1^2 * y_0^2 y_1^4 = y_0 y_1^2
    or
    x_0 * y^1 = 0

    This function implements this operation. For example:
    sage: R.<x0, x1, x2> = QQ[]
    sage: contract(5*x0^2*x1^3, 3*x0*x1^2)
    15*x0*x1
    '''

    R = m.parent()
    d = R(d)
    mdict = m.dict()
    ddict = d.dict()
    outd = {}
    for dmon in ddict:
        for mmon in mdict:
            contracted = tuple((x-y for (x, y) in zip(mmon, dmon)))
            if all((z >= 0 for z in contracted)):
                coeff = ddict[dmon]*mdict[mmon]
                outd[contracted] = outd.get(contracted, 0) + coeff
    return R(outd)

def all_monomials(vars, deg):
    '''
    all_monomials(vars, deg) -- return all monomials in given vars of
    a given degree

    For example:
    sage: R.<x0, x1, x2> = QQ[]
    sage: all_monomials([x0,x1,x2], 3)
    [x0^3, x0^2*x1, x0*x1^2, x1^3, x0^2*x2, x0*x1*x2, x1^2*x2,
     x0*x2^2, x1*x2^2, x2^3]
    '''

    n = len(vars)
    vs = WeightedIntegerVectors(deg, n*[1])
    mons = []
    for vec in vs:
        m = 1
        for var, exp in zip(vars, vec):
            m *= var^exp
        mons.append(m)
    mons.reverse()
    return mons

def exponents_to_str(exps):
    v = ""
    for i, e in enumerate(exps):
        v += str(i)*e
    return v
    
def mon_to_str(mon):
    exps = mon.exponents()[0]
    return exponents_to_str(exps)

def exponents_to_dual_str(exps):
    return "phi_%s" % exponents_to_str(exps)
    
def mon_to_dual_str(mon):
    '''
    exponents_to_dual_str(exps) -- converts a tuple of exponents of a
    monomial at given variable to the name of an element in a dual
    basis

    Let V be a vector space with basis x0, ..., xn. Having chosen a
    basis, we obtain an isomorphism of symmetric power S^k V with a
    vector space of degree k homogenous polynomials in variables x0,
    ..., xn. Naturally, degree k monomials in variables x0, ..., xn
    form a basis of S^k V.

    For us, it will be useful to work also with a dual space to S^k V
    -- indeed, the degree k Veronese reembedding embeds PV* as a
    subvariety of P (S^k V)*. This function lets us obtain a name of a
    dual basis element dual to a given monomial in S^k V. For example,

    sage: mon_to_dual_str(x0^2 * x1 * x2)
    'phi_0012'
    '''

    exps = mon.exponents()[0]
    return exponents_to_dual_str(exps)

def dual_vars_and_mons(vars, deg):
    mons = all_monomials(vars, deg)
    return [(mon_to_dual_str(mon), mon) for mon in mons]

def dual_var_names(vars, deg):
    '''
    dual_var_names(vars, deg) -- returns a list of names of vectors in
    dual basis to a standard basis in S^deg span<vars>

    For example:
    sage: dual_var_names([x0,x1,x2], 3)
    ['phi_000', 'phi_001', 'phi_011', 'phi_111', 'phi_002',
     'phi_012', 'phi_112', 'phi_022', 'phi_122', 'phi_222']
    '''

    return [vm[0] for vm in dual_vars_and_mons(vars, deg)]

def polynomial_to_dual(poly, vars):
    '''
    polynomial_to_dual(poly, vars) -- converts a homogenous polynomial
    to a dual representation

    For example, generic degree 3 form in variables x0, x1, x2 has
    form
    phi_000 x0^3 + phi_111 x1^3 + phi_222 x2^3 +
    + 3 phi_001 x0^2 x1 + 3 phi_112 x1^2 x2 + 3 phi_022 x2^2 x0 +
    + 6 phi_012 x0 x1 x2

    This functions converts a polynomial in variables vars to the
    dictionary of its dual coefficients. Remember, though, that these
    are not exactly the coefficients in the dual basis, as we also
    divide by multinomial coefficients.

    For example:
    sage: polynomial_to_dual((x0+x1+x2)^3, [x0,x1,x2])
    {'phi_000': 1, 'phi_001': 1, 'phi_002': 1, 'phi_011': 1,
    'phi_012': 1, 'phi_022': 1, 'phi_111': 1, 'phi_112': 1,
    'phi_122': 1, 'phi_222': 1} 

    sage: polynomial_to_dual(x0^2 + x0 * x1 + 3 * x0 * x2, [x0,x1,x2])
    {'phi_00': 1, 'phi_01': 1/2, 'phi_02': 3/2,
     'phi_11': 0, 'phi_12': 0, 'phi_22': 0}
    '''

    assert poly.is_homogeneous()
    deg = poly.degree()
    pd = poly.dict()
    var_names = dual_var_names(vars, deg)
    v = dict(zip(var_names, [0]*len(var_names)))
    for mon_exps in pd:
        # we divide by multinomial, because our basis in dual is
        # x_{a_1}^{b_1} * ... * x_{a_k}^{b^k} |-->
        # |-- > (deg \choose b_1, b_2, ..., b_k) phi_{a_1(b_1 times) ... a_k(b_k times)}
        # e.g. x_0^2 x_2 |--> (3 \choose 2, 1) phi_002 = 3 phi_002
        multi = multinomial(*mon_exps)
        cf = pd[mon_exps]/multi

        dual_var_name = exponents_to_dual_str(mon_exps)
        v[dual_var_name] += cf
    return v

def random_linear_polynomial(vars, hi=10):
    '''
    random_linear_polynomial(vars, hi=10) -- returns a random linear
    combination of variables in vars, with integral coefficients from
    1 to hi (default from 1 to 10)

    For example:
    sage: random_linear_polynomial([x0,x1,x2])
    x0 + 2*x1 + 7*x2
    sage: random_linear_polynomial([x0,x1,x2])
    6*x0 + 4*x1 + 5*x2
    '''

    r = 0
    for v in vars:
        # we don't want to get 0 here as it would be nongeneric
        r += ZZ.random_element(1, hi)*v
    return r


def generic_element_of_veronese_secant(vars, d, k):
    '''
    generic_element_of_veronese_secant(vars, d, k) -- returns a
    generic element of k-th secant variety to degree d Veronese
    reembedding of P(span<vars>*)

    Degree d Veronese reembedding of PV* embeds its as a subvariety of
    P (S^d V)*. Form f \in (S^d V)* is in the image of degree d
    Veronese precisely when it is a d-th power of a linear form. Thus,
    a generic element of k-th secant variety to degree k Veronese
    embedding is precisely a form which is a sum of k d-th powers of
    linear forms.

    Since the projective space of lines in V is PV*, we need to
    convert a sum of k d-th powers to its dual representation, and
    since our dual basis in V* is not exactly the basis dual to the
    monomials, we also divide by appropriate multinomial coefficient.
    '''

    f = 0
    for i in range(k):
        f += random_linear_polynomial(vars)^d
    return polynomial_to_dual(f, vars)

def general_homogeneous_form(vars, deg):
    '''
    general_homogeneous_form(vars, deg) -- returns a general
    homogenous form in variables vars of degree deg.

    For example:
    sage: general_homogeneous_form([x0,x1,x2], 3)
    phi_000*x0^3 + phi_001*x0^2*x1 + phi_011*x0*x1^2 + phi_111*x1^3 +
    + phi_002*x0^2*x2 + phi_012*x0*x1*x2 + phi_112*x1^2*x2 +
    + phi_022*x0*x2^2 + phi_122*x1*x2^2 + phi_222*x2^3
    '''

    mons = all_monomials(vars, deg)
    lm = len(mons)

    old_vars = map(str,vars)
    lv = len(old_vars)

    gen_vars = ["phi_%s" % mon_to_str(mon) for mon in mons]
    Sbase = PolynomialRing(vars[0].base_ring(), names = gen_vars)
    Sgen = PolynomialRing(Sbase, names = old_vars)
    return sum((Sgen(Sbase.gen(i))*Sgen(mons[i]) for i in range(lm)))

def catalecticant(f, vars, a, b):
    '''
    catalecticant(f, vars, a, b) -- given f \in S^d span<x0, ..., xk>,
    calculates a matrix of a corresponding catalecticant
    f_a,b: S^a span<x0, ..., xk> -> S^b span<x0, ..., xk>

    Obviously, for it to make sense we must have a+b = deg f.

    The result is (a+k \choose a) x (b+k \choose b) matrix
    representing the catalecticant in the natural bases of S^a
    span<x0, ..., xk> and S^b<x0, ..., xk>, that is, the bases of
    degree a and degree b monomials respectively.

    For example:
    sage: f = general_homogeneous_form([x0,x1,x2], 3)
    sage: f
    phi_000*x0^3 + phi_001*x0^2*x1 + phi_011*x0*x1^2 + phi_111*x1^3
    + phi_002*x0^2*x2 + phi_012*x0*x1*x2 + phi_112*x1^2*x2 + phi_022*x0*x2^2
    + phi_122*x1*x2^2 + phi_222*x2^3
    sage: cf = contract(f, x0)
    sage: cf
    phi_000*x0^2 + phi_001*x0*x1 + phi_011*x1^2 + phi_002*x0*x2
    + phi_012*x1*x2 + phi_022*x2^2
    sage: catalecticant(cf, [x0,x1,x2], 1, 1)
    [phi_000 phi_001 phi_002]
    [phi_001 phi_011 phi_012]
    [phi_002 phi_012 phi_022]
    '''

    ma = all_monomials(vars, a)
    la = len(ma)
    mb = all_monomials(vars, b)
    lb = len(mb)
    assert f.is_homogeneous()
    assert f == 0 or f.degree() == a+b
    S = f.parent()
    rows = []
    for i in range(la):
        row = []
        for j in range(lb):
            monij = ma[i]*mb[j]
            row.append(f.monomial_coefficient(S(monij)))
        rows.append(row)
    return matrix(rows)

def calculate_pev(pe, vars, bjs, ais, d):
    '''
    calculate_pev(pe, vars, bjs, ais, d) -- calculate the matrix of a
    natural morphism P^E_v: H^0(L_1) -> H^0(Hom(L_0, L))* coming from
    the presentation p_E: L_1 -> L_0 of a vector bundle E.

    Let p_E: L_1 -> L_0 be a presentation of a vector bundle E -- that
    is, let L_1 = (+)_j O(b_j), L_0 = (+)_i O(a_i) be direct sums of
    line bundles on P^n, and let p_E: L_1 -> L_0 be a map between them
    such that im p_E is a vector bundle E. Let also L = O(d) be some
    very ample line bundle on P^n, and let v be any element of
    H^0(L)*. Then we have a natural map

      P^E_v: H^0(L_1) -> H^0(Hom(L_0, L))*

    defined as:

      P^E_v(s)(phi) = v(phi(p_E(s)))

    This function calculates the matrix of P^E_v in natural bases of
    H^0(L_1) and H^0(Hom(L_0, L))*.
    `pe' is a matrix that represents a presentation map p_E
    `vars' are projective coordinates, i.e. basis of H^0(O(1))
    `bjs' and ais are lists of degrees of line bundles being summands of
    `L_1' and `L_0', respectively
    `d' is a degree of a bundle L that is used here.

    For example:
    sage: R.<x0, x1, x2> = QQ[]
    sage: pe = matrix([[0 , x2, -x1], [-x2, 0, x0], [x1, -x0, 0]])
    sage: pe
    [  0  x2 -x1]
    [-x2   0  x0]
    [ x1 -x0   0]
    sage: pev = calculate_pev(pe, [x0,x1,x2], [1,1,1], [2,2,2], 3)
    sage: pev
    [       0        0        0  phi_002  phi_012  phi_022 -phi_001 -phi_011 -phi_012]
    [       0        0        0  phi_012  phi_112  phi_122 -phi_011 -phi_111 -phi_112]
    [       0        0        0  phi_022  phi_122  phi_222 -phi_012 -phi_112 -phi_122]
    [-phi_002 -phi_012 -phi_022        0        0        0  phi_000  phi_001  phi_002]
    [-phi_012 -phi_112 -phi_122        0        0        0  phi_001  phi_011  phi_012]
    [-phi_022 -phi_122 -phi_222        0        0        0  phi_002  phi_012  phi_022]
    [ phi_001  phi_011  phi_012 -phi_000 -phi_001 -phi_002        0        0        0]
    [ phi_011  phi_111  phi_112 -phi_001 -phi_011 -phi_012        0        0        0]
    [ phi_012  phi_112  phi_122 -phi_002 -phi_012 -phi_022        0        0        0]

    '''

    gf = general_homogeneous_form(vars, d)
    rows = []
    for j, bj in enumerate(bjs):
        row = []
        for i, ai in enumerate(ais):
            pij = pe[i][j]
            c = catalecticant(contract(gf, pij), vars, bj, d-ai)
            row.append(c)
        rows.append(row)
    return block_matrix(rows, subdivide=False).transpose()


def find_rank_conormal_pairing(pev, v, vars, deg):
    '''
    find_rank_conormal_pairing(pev, v, vars, deg) -- finds the rank of a
    conormal space to rank variety at v.

    The function calculate_pev defined above calculates a matrix of a
    map
      P^E_v: H^0(L_1) -> H^0(Hom(L_0, L))*
    originating form a presentation p_E: L_1 -> L_0 of a vector bundle
    E of rank e.

    Consider a rank k variety corresponding to P^E_v:

      Rank_k(E) = { [v] \in P H^0(L)*: rank P^E_v <= e*k }

    The theorem says (under some assumptions), that if we embed the
    variety X using line bundle L, its r-th secant variety
    sigma_r(X) \subset P H^0(L)* is contained in Rank_r(E) \subset P H^0(L)*.

    Another theorem gives (under some assumptions) a condition for
    sigma_r(X) to be an irreducible component of Rank_r(E). Indeed, we
    have a pairing

      g_v: ker P^E_v (x) (im P^E_v)^T -> H^0(L)**

    defined by:
      g_v(f, h)(phi) = P^E_phi(f)(h)

    For a smooth point [v] \in Rank_r(E), the image of g_v, mapped by
    natural isomorphism H^0(L)** -> H^0(L), is precisely the affine
    conormal space of Rank_r(E). The theorem says that if v \in
    sigma_k(X) is a smooth point of Rank_k(E), and the rank of g_v is
    equal to the codimension of sigma_k(X), then sigma_k(X) is an
    irreducible component of Rank_r(E) passing through [v].

    This function, given matrix `pev' repesenting a map P^E_v, a vector
    `v' \in H^0(L), projective coordinates `vars' of a projective space
    P^n, and the degree `deg' of Veronese reembedding of P^n,
    calculates the rank of a conormal map. One can then compare it
    with the codimension of sigma_k(v_deg(P^n)).
    '''

    gvars = dual_var_names(vars, deg) # names of vectors in basis of H^0(L)*
    gvn = len(gvars)
    H0LDD = QQ^gvn
    zerop = dict(zip(gvars, [0]*gvn)) # zero functional in H^0(L)*

    hld_basis = [] # basis of H^0(L)*
    for gv in gvars:
        hld_v = zerop.copy()
        hld_v[gv] = 1
        hld_basis.append(hld_v)

    pv = matrix(QQ, pev(**v)) # P_v

    # in Sage, .kernel() is "left kernel" e.g. { w: wA = 0 }
    # so m.kernel() is actually kernel of the transpose of m
    ker_pv_basis = pv.transpose().kernel().basis()
    im_pv_orth_basis = pv.kernel().basis()
    conormal_gens = []
    # now iterate over all pairs
    A = pev.base_ring()
    for f in ker_pv_basis:
        for h in im_pv_orth_basis:
            l = [0]*gvn
            g_fh = (matrix(A, h) * pev * matrix(A, f).transpose())[0][0]
            for i, hlv in enumerate(hld_basis):
                l[i] = g_fh(**hlv)
            conormal_gens.append(H0LDD(l))
    return H0LDD.span(conormal_gens)
